# Legend of the Five Rings (5th Edition) - Family Presentation
![Banner Legend of the Five Rings](./l5rBan.jpg)

[![Buy Me a Coffee](https://img.shields.io/badge/Buy%20Me%20a-☕%20Coffee-red)](https://ko-fi.com/vlyan)
[![System version](https://img.shields.io/badge/L5R5e-v1.9.x+-green)](https://foundryvtt.com/packages/l5r5e)
[![FoundryVTT version](https://img.shields.io/badge/FVTT-v10/v11.x-informational)](https://foundryvtt.com/)

Bienvenue dans ce module fanmade de L5R 5ième édition qui décrit toutes les familles des Clans Majeurs et Mineurs de Rokugan, ainsi que leur mon et devise.

De plus, chaque mon est disponible séparément dans le module pour faciliter le drag & drop.

## Comment installer ce module ?
Pour installer ce module, vous devez :

1) Copier ce lien et chargez-le dans le menu "Module" depuis votre interface Foundry.
> https://gitlab.com/teaml5r/l5r5e-family-presentation/-/raw/master/module/module.json

2) Une fois dans votre monde, activez le module "Présentation des familles". Foundry vous indique qu'un second module est requis. Il s'agit du module "Compendium-Folder" qui permet de classer les familles par dossier.


![Presentation des familles](./ScreenPresentationFamille.jpg)


## Update/Mise à jour
Date format : day/month/year
- 29/05/2023 - 1.0.3 : FoundryVTT v11 compatibility
- 19/05/2023 - 1.0.2 : FoundryVTT v10 compatibility
- 14/09/2021 - 1.0.1 : Correction depuis les retours de Gilthyon
- 06/09/2021 - 1.0.0 : Initial release
